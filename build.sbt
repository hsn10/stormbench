scalaVersion:="2.10.4"

libraryDependencies ++= Seq (
    "com.github.velvia" %% "scala-storm" % "0.2.4-SNAPSHOT",
    "org.apache.storm" % "storm-core" % "0.9.2-incubating" % "provided"
)
