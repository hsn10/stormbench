package com.filez.storm.ping

import backtype.storm.tuple.Tuple
import storm.scala.dsl._

class Pinger extends StormSpout(outputFields = List("ping")) {

   var total:Long = 0

   setup {
       total = 0
   }

   shutdown {
       println(s"$total messages emited.")
   }

   override
   def nextTuple = {
     total += 1
     msgId(total).emit(System.currentTimeMillis():java.lang.Long)
   }
}

class Rtt extends StormBolt(outputFields = List("rtt")) {

  override
  def execute(t: Tuple) = {
     val now = System.currentTimeMillis()
     val rtt = now - t.getLong(0)
     t.emit(rtt)
     t.ack
  }
}

class RttReporter extends StormBolt(outputFields = List()) {
    private var total:Int = _
    private var rttTotal:Long = _
    private var batch:Int = _
    private var batchStart:Long = _

    private val BATCH = 20000

    setup { 
	total = 0
	rttTotal = 0
	newBatch()
    }

    override
    def execute(t: Tuple) = {
	total += 1
	batch += 1
	val rtt = t.getLong(0)
	rttTotal += rtt
	
	if (batch % BATCH == 0) {
	    val now = System.currentTimeMillis()
	    println(s"$total, rtt ${rtt}ms, avg rtt ${rttTotal/total}, speed=${BATCH/((now-batchStart)/1000.0)}")
	    newBatch()
	}
	t.ack
    }

    private def newBatch() {
        batch = 0
	batchStart = System.currentTimeMillis()   
    }
}
