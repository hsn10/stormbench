package com.filez.storm.ping

import backtype.storm.topology.TopologyBuilder
import backtype.storm.{StormSubmitter,Config}

object Driver extends App {
    val builder = new TopologyBuilder()
    val conf = new Config()
    builder.setSpout("pinger", new Pinger(), 1)
    builder.setBolt("rtt", new Rtt(), 2).shuffleGrouping("pinger")
    builder.setBolt("reporter", new RttReporter(), 1).localOrShuffleGrouping("rtt")
    conf.setNumWorkers(3)
    conf.setMaxSpoutPending(10000)
    conf.setNumAckers(1)

    StormSubmitter.submitTopology("Storm-throughput-test", conf, builder.createTopology())
}
